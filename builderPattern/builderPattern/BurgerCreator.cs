﻿using System;
using System.Collections.Generic;
using System.Text;

namespace builderPattern
{
    class BurgerCreator
    {
        private IBurgerBuilder _burgerBuilder;

        //public IBurgerBuilder builder
        //{
          //  set { _burgerBuilder = value; }
        //}

        public BurgerCreator(IBurgerBuilder burgerBuilder) 
        {
            _burgerBuilder = burgerBuilder;
        }

        public void CreateBurger()
        {
            _burgerBuilder.SetBuns();
            _burgerBuilder.SetPatty();
            _burgerBuilder.SetSauce();
            _burgerBuilder.SetLettuce();
            _burgerBuilder.SetCheese();
        }

        public Burger GetBurger()
        {
            return _burgerBuilder.GetBurger();
        }
    }
}
