﻿using System;
namespace builderPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("select 1 for Chicken Burger or 2 for Beef Burger or 3 for Zinger Burger");
            string menu = Console.ReadLine();
            if (menu == "1")
            {
                var chickenBurgerCreator = new BurgerCreator(new ChickenBurgerBuilder());
                chickenBurgerCreator.CreateBurger();
                Console.WriteLine(chickenBurgerCreator.GetBurger().Buns);
                Console.WriteLine(chickenBurgerCreator.GetBurger().Patty);
                Console.WriteLine(chickenBurgerCreator.GetBurger().Sauce);
                Console.WriteLine(chickenBurgerCreator.GetBurger().Lettuce);
                Console.WriteLine(chickenBurgerCreator.GetBurger().Cheese);
            }
            else if (menu == "2")
            {
                var beefBurgerCreator = new BurgerCreator(new BeefBurgerBuilder());
                beefBurgerCreator.CreateBurger();
                Console.WriteLine(beefBurgerCreator.GetBurger().Buns);
                Console.WriteLine(beefBurgerCreator.GetBurger().Patty);
                Console.WriteLine(beefBurgerCreator.GetBurger().Sauce);
                Console.WriteLine(beefBurgerCreator.GetBurger().Lettuce);
                Console.WriteLine(beefBurgerCreator.GetBurger().Cheese);
            }
            else if (menu == "3")
            {
                var ZingerBurgerCreator = new BurgerCreator(new ZingerBurgerBuilder());
                ZingerBurgerCreator.CreateBurger();
                Console.WriteLine(ZingerBurgerCreator.GetBurger().Buns);
                Console.WriteLine(ZingerBurgerCreator.GetBurger().Patty);
                Console.WriteLine(ZingerBurgerCreator.GetBurger().Sauce);
                Console.WriteLine(ZingerBurgerCreator.GetBurger().Lettuce);
                Console.WriteLine(ZingerBurgerCreator.GetBurger().Cheese);
            }
            else
            {
                Console.WriteLine("wrong input");
                
            }

        }
    }
}
