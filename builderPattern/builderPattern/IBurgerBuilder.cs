﻿using System;
using System.Collections.Generic;
using System.Text;

namespace builderPattern
{
    interface IBurgerBuilder
    {
        void SetBuns();
        void SetPatty();
        void SetSauce();
        void SetLettuce();
        void SetCheese();
        Burger GetBurger();

    }
}
