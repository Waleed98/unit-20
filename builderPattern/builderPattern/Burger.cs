﻿using System;
using System.Collections.Generic;
using System.Text;

namespace builderPattern
{
    class Burger
    {
        public string Buns
        {
            get;
            set;
        }

        public string Patty
        {
            get;
            set;
        }

        public string Sauce
        {
            get;
            set;
        }
        public string Lettuce
        {
            get;
            set;
        }
        public string Cheese
        {
            get;
            set;
        }

        public string ShowBurger()
        {
            return Buns+Patty+Sauce+Lettuce+Cheese;
        }
    }
}
