﻿using System;
using System.Collections.Generic;
using System.Text;

namespace builderPattern
{
    class ChickenBurgerBuilder:IBurgerBuilder
    {
        Burger burger = new Burger();
        public void SetBuns()
        {
            burger.Buns = "Round Bun";
        }
        public void SetPatty()
        {
            burger.Patty = "Chicken burger patty";
        }
        public void SetSauce()
        {
            burger.Sauce = "Chicken BBQ Sauce";
        }
        public void SetLettuce()
        {
            burger.Lettuce = "Lettuce";
        }
        public void SetCheese()
        {
            burger.Cheese = "Chedder Cheese";
        }

        public Burger GetBurger()
        {

            return burger;
        }
    }
}
