﻿using System;
using System.Collections.Generic;
using System.Text;

namespace builderPattern
{
    class ZingerBurgerBuilder: IBurgerBuilder
        {
            Burger burger = new Burger();
            public void SetBuns()
            {
                burger.Buns = "Round Bun";
            }
            public void SetPatty()
            {
                burger.Patty = "Zinger burger patty";
            }
            public void SetSauce()
            {
                burger.Sauce = "Zinger BBQ Sauce";
            }
            public void SetLettuce()
            {
                burger.Lettuce = "Lettuce";
            }
            public void SetCheese()
            {
                burger.Cheese = "Chedder Cheese";
            }

            public Burger GetBurger()
            {

                return burger;
            }
        }
    }



