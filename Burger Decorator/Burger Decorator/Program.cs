﻿using System;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            PlainBurger plainburger = new PlainBurger();
            string Plainburger = plainburger.MakeBurger();
            Console.WriteLine(plainburger);

            BurgerDecorator chickenBurgerDecorator = new chickenBurgerDecorator(plainburger);
            string chickenBurger = chickenBurgerDecorator.MakeBurger();
            Console.WriteLine("\n'" + chickenBurger);

            ZingerBurgerDecorator zingerBurgerDecorator = new ZingerBurgerDecorator(plainburger);
            string zingerBurger = zingerBurgerDecorator.MakeBurger();
            Console.WriteLine("\n'" + zingerBurger);

            BeefBurgerDecorator BeefPizzaDecorator = new BeefBurgerDecorator(plainburger);
            string beefBurger = BeefPizzaDecorator.MakeBurger();
            Console.WriteLine("\n'" + beefBurger);
        }
        public interface Burger
        {
            string MakeBurger();
        }
        public class PlainBurger : Burger
        {
            public string MakeBurger()
            {
                return "Plain Burger";
            }
        }
        public abstract class BurgerDecorator : Burger
        {
            protected Burger burger;
            public BurgerDecorator(Burger burger)
            {
                this.burger = burger;
            }
            public virtual string MakeBurger()
            {
                return burger.MakeBurger();
            }
        }
        public class chickenBurgerDecorator : BurgerDecorator
        {
            public chickenBurgerDecorator(Burger burger) : base(burger)
            {
            }
            public override string MakeBurger()
            {
                return burger.MakeBurger() + AddChicken() + AddExtraSauce();
            }
            private string AddChicken()
            {
                return ", Chicken added ";
            }
            private string AddExtraSauce()
            {
                return ", Extra Sauce added";
            }

        }
        public class ZingerBurgerDecorator : BurgerDecorator
        {
            public ZingerBurgerDecorator(Burger burger) : base(burger)
            {
            }
            public override string MakeBurger()
            {
                return burger.MakeBurger() + AddZinger() + AddExtraCheese();
            }
            private string AddZinger()
            {
                return ", Zinger added";
            }
            private string AddExtraCheese()
            {
                return ", Extra cheese added";
            }
        }
        public class BeefBurgerDecorator : BurgerDecorator
        {
            public BeefBurgerDecorator(Burger burger) : base(burger)
            {
            }
            public override string MakeBurger()
            {
                return burger.MakeBurger() + AddBeef();

            }
            private string AddBeef()
            {
                return ", Beef added";
            }
        }

    }
}
